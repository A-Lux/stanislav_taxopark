<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Card\CardController;
use App\Http\Controllers\Api\Pay\PaymentController;
use App\Http\Controllers\Api\Pay\WithdrawController;
use App\Http\Controllers\Api\Pay\StatusController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\NotificationController;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/',function(){
    $query = new \stdClass();
    $park = new stdClass();
    $park->id = env('PARK_ID');
    $query->park = $park;
    $login = new LoginController();
    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'X-Client-ID' => $login->client_id,
        'X-Api-Key' => $login->api_key,
        'Content-Type' => 'application/json',
        'Accept-Language' => 'ru',
        'X-Idempotency-Token' => (string) Str::uuid()
    ])->post('https://fleet-api.taxi.yandex.net/v2/parks/transactions/categories/list',[
        'query' => $query
    ]);

    return response(['resonse' => json_decode($response->body())],200);

});
Route::post('/token/register',[NotificationController::class,'register']);
Route::prefix('register')->group(function(){
    Route::post('/', [RegisterController::class, 'register']);
    Route::post('/check',[RegisterController::class,'checkCode']);
});
Route::prefix('login')->group(function(){
    Route::post('/',[LoginController::class,'login']);
    Route::post('/check',[LoginController::class,'checkCode']);
});
Route::post('/accept/withdraw/status',[StatusController::class,'withdrawStatus'])->name('withdrawStatus');
Route::post('/accept/payment/status',[StatusController::class,'paymentStatus'])->name('paymentStatus');
Route::get('/get/all/users',[LoginController::class,'registerAllUsers']);
Route::post('/accept/card',[CardController::class,'acceptCard'])->name('acceptCard');
Route::middleware('auth:api')->group(function(){
    Route::get('user',[LoginController::class,'getUserData']);
    Route::get('orders',[OrderController::class,'orders']);
    Route::prefix('card')->group(function(){
        Route::get('/',[CardController::class,'getCards']);
        Route::post('/link',[CardController::class,'linkCard']);
        Route::post('/delete',[CardController::class,'deleteCard']);
    });
    Route::post('/update/avatar',[LoginController::class,'updateAvatar']);
    Route::post('/logout',[LoginController::class,'logout']);
    Route::middleware('throttle:1,10')->group(function() {

        Route::prefix('withdraw')->group(function () {
            Route::post('/', [WithdrawController::class, 'withdraw']);
        });
        Route::post('/without/cards', [WithdrawController::class, 'withoutCard']);

    });


    Route::prefix('pay')->group(function(){
        Route::post('/',[PaymentController::class,'pay']);

    });
});
