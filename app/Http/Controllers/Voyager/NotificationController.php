<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class NotificationController extends VoyagerBaseController
{
    protected $server_key = 'AAAAMFAGw7A:APA91bEyulcFyN4w9EGd9ctP5J8-Vz8BN3ZKvwtS0J8Vc2agP5fxCfw6LS4-SeEqpSHVUXFrhWkmbvsX8oLQM4Nvo8jQBUVSxPWFefLUlD2gtfceBmbfhaNhXKSrU21zgphg5r_W7fkr';
    //
    public function store(Request $request)
    {
        $notification = Notification::create([
            'user_id' => $request->has('user_id') ? $request->user_id : null,
            'text' => $request->text,
            'for_all' => $request->for_all != null ? 1 : 0
        ]);

        if ($notification->for_all != null){
            $user_device = UserDevice::get();
            foreach ($user_device as $device){
                $this->notify($device,$request);
            }
        }else{
            $user = User::find($request->user_id);
            foreach ($user->devices as $device){
                $this->notify($device,$request);
            }
        }

        return back()->with('message','Уведомление отправлено');
    }


    public function notify(UserDevice $user_device,$request)
    {
        $data = [
            "to" => $user_device->device_id,
            "notification" => [
                "title" => "Таксопарк ААА",
                "body" => $request->text,
                "badge" => 1,
                "icon" => asset('/images/logo.jpg'),
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $this->server_key,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        curl_exec($ch);

    }
}
