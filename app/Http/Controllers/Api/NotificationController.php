<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserDevice;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //

    public function register(Request $request)
    {
        $valData = $request->validate([
            'device_id' => 'required',
        ]);
        $userDevice = UserDevice::where('device_id',$valData['device_id'])->first();
        if (!$userDevice){
            $userDevice = UserDevice::create([
                'device_id' => $valData['device_id']
            ]);
        }

        return response(['device' => $userDevice],201);


    }

}
